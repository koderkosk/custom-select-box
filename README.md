# Chuck Norris facts reader

Retrieve a random chuck joke.

# Getting Started

## Demo link: 

[http://custom-sb.janc.sk/](http://custom-sb.janc.sk/)

![Preview](./images/app.jpg)

# Installation

## Cascading Style Sheets

CSS file "[page.css](./css/page.css)" is generated with LESS from files:

- [normalize.less](./css/normalize.less) //cross-browser reset
- [page.less](./css/page.less)

```
<link href="./css/page.css" rel="stylesheet">
```

## Javascript

JS file "[main.min.js](./js/main.min.js)" compressed file from files:

- [preloader.js](./js/preloader.js) //page preloader
- [ie-fix.js](./js/ie-fix.js) //hotfix IE
- [component-1.js](./js/component-1.js) //task 1
- [component-2.js](./js/component-2.js) //task 2

```
<script src="./js/main.min.js"></script>
```
### preloader.js

[preloader.js](./js/preloader.js) is standart web page preloader.

### ie-fix.js

[ie-fix.js](./js/ie-fix.js) fixing bugs in IE. IE have problem with "forEach" loop and "customEvent". Thanks to Stack Overflow.

### component-1.js

[component-1.js](./js/component-1.js) is first task. I created a custom select box component with custom design.

![Preview](./images/component-1.jpg)

### component-2.js

[component-2.js](./js/component-2.js) is second task. I created a XHR request to Chuck noris api which communicate via customEvent with component 1.

![Preview](./images/component-2.jpg)

### SVG sprite

SVG sprite must be used as inline SVG. IE 9, 10, or 11 don't support external source.

![Preview](./images/svg-sprite.jpg)

### HTML usage

```
<link href="./css/page.css" rel="stylesheet">

<!-- first select box -->
<div class="custom-select-box">
   <form action="#">
      <select name="category">
            <option value="0">Select options:</option>
            <option value="1">Movie</option>
            <option value="2">Food</option>
            <option value="3">Travel</option>
      </select>
   </form>
</div>

<!-- N box -->
<div class="custom-select-box">
   <form action="#">
      <select name="second">
            <option value="0">Select options:</option>
            <option value="1">Music</option>
            <option value="2">Career</option>
            <option value="3">Fashion</option>
      </select>
   </form>
</div>

<script src="./js/main.min.js"></script>

```

## File structure
```
custom-select-box
├── css
│   ├── normalize.css
│   ├── normalize.less
│   ├── page.css
│   └── page.less
├── images
│   ├── svg
│   │    └── ok.svg
│   ├── fb.png
│   └── preloader.jpg
├── js
│   ├──component-1.js
│   ├──component-2.js
│   ├──ie-fix.js
│   ├──main.min.js
│   └──preloader.js
├── favicon.ico
├── index.html
├── robots.txt
└── README.md
```

## Tested in

### Windows

- Firefox
- Chrome
- IE 10+
- Opera

## Versioning

For the versions available, see the [tags on this repository](https://bitbucket.org/koderkosk/custom-select-box/commits/all). 

## Author

* **Tomáš Janč** - *Initial work* - [Very OLD Portfolio](http://shortcv.janc.sk)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details