var container = document.querySelectorAll(".custom-select-box"); //find custom box wrapper

container.forEach(function(value, i) {

	var select = container[i].querySelectorAll("select"); //find tag select

	//if selectbox exists
	if(select.length > 0) {

		for (var j = 0; j < select.length; j++) {

			var option = select[j].querySelectorAll("option"); //select all html option
			
			//create selected element
			var divSelected = document.createElement("div");
				divSelected.setAttribute("class", "select-box");
				container[i].appendChild(divSelected);

				divSelected.addEventListener("click", function(el) {
					el.stopPropagation();
				    closeList(this);
				 });

			//create list	
			var list = document.createElement("ul");
				list.setAttribute("class", "select-items");
				container[i].appendChild(list);

			var li;

			//generate
			option.forEach(function(element) {

				//append text from selected item
				if(element.selected) {
					divSelected.innerHTML = element.text;
				} 

				//create list items
				li = document.createElement("li");
				li.setAttribute('role', 'tab');
				li.setAttribute('aria-labelledby', 'select' + element.value);
				li.setAttribute('tabindex', 0);
				li.innerHTML = element.text;
				list.appendChild(li);

				//select item
				li.addEventListener("click", function(el) {
					el.preventDefault();
					//remove all selected lists
					var allOptions = this.parentNode.getElementsByTagName('li');
					for (var k = 0; k < allOptions.length; k++) {
		              allOptions[k].removeAttribute("class");
		            }
				    this.setAttribute("class", "active"); //add active class to li
				    divSelected.innerHTML = this.innerHTML;
				    element.selected = true; //seleced - standart html selectbox
				    getJoke(this.innerHTML.toLowerCase());
				 });

			});

		};

 	}		

});

//close list
function closeList(el) {

	if(el) {
		var x = el.nextSibling.style.display === "block" ? true : false;
	}

	var closeAll = document.querySelectorAll(".custom-select-box ul");

	for (var i = 0; i < closeAll.length; i++) {
		closeAll[i].style.display = 'none';
	}

	if(el && x === false) {
		el.nextSibling.style.display = "block";
	}
}

//click outside selectbox
document.addEventListener("click", function() {
	closeList();
});